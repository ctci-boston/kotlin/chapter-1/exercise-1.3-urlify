fun urlify(s: String): String {
    fun countSpaces(): Int {
        var result = 0
        s.forEach { char -> if (char == ' ') result++ }
        return result
    }
    fun urlify(): String {
        val spaceCount = countSpaces()
        val buffer = CharArray(s.length + 2 * spaceCount)
        var p1 = s.length - 1
        var p2 = p1 + 2 * spaceCount
        fun mapSpace() {
            buffer[p2--] = '0'
            buffer[p2--] = '2'
            buffer[p2--] = '%'
        }
        while (p1 >= 0) {
            val c = s[p1--]
            when (c) {
                ' ' -> mapSpace()
                else -> buffer[p2--] = c
            }
        }
        return String(buffer)
    }

    return when {
        s.isEmpty() || countSpaces() == 0 -> s
        else -> urlify()
    }
}
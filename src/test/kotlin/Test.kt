import org.junit.jupiter.api.Assertions.assertEquals
import kotlin.test.Test

class Test {

    @Test fun `When the empty string is urlified return the empty string`() {
        val expected = ""
        val actual = urlify("")
        assertEquals(expected, actual)
    }

    @Test fun `When there are no spaces in the input string return the input string`() {
        val inputString = "http://abcd"
        val expected = inputString
        val actual = urlify(inputString)
        assertEquals(expected, actual)
    }

    @Test fun `When there is one space in the input string return the urlified input string`() {
        val inputString = "http://abc xyz"
        val expected = "http://abc%20xyz"
        val actual = urlify(inputString)
        assertEquals(expected, actual)
    }

    @Test fun `When there are three spaces in the input string return the urlified input string`() {
        val inputString = "http://abc def xyz 123"
        val expected = "http://abc%20def%20xyz%20123"
        val actual = urlify(inputString)
        assertEquals(expected, actual)
    }

    @Test fun `When there are two consecutive spaces in the input string return the urlified input string`() {
        val inputString = "http://abc  xyz"
        val expected = "http://abc%20%20xyz"
        val actual = urlify(inputString)
        assertEquals(expected, actual)
    }

    @Test fun `When there are spaces at the boundaries of the input string return the urlified input string`() {
        val inputString = " http://abc "
        val expected = "%20http://abc%20"
        val actual = urlify(inputString)
        assertEquals(expected, actual)
    }
}
# exercise-1.3-urlify

Provides a Kotlin solution for replacing space characters in a URL with the escape sequence "%20"